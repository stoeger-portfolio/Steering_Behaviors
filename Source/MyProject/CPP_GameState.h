// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "CPP_GameState.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT_API ACPP_GameState : public AGameStateBase
{
	GENERATED_BODY()
	
public:
	
	ACPP_GameState();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 NumberOfEnemiesAlive;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bLevelCompleted;

	UFUNCTION()
	void EnemyKilled();
};
